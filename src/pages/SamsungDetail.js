import { Card, CardContent, Container, CardActions, Button, Typography } from "@mui/material"
import LabelTitle from "../components/labelTitle"

const SamsungDetail = () => {
    return (
        <Container>
            <LabelTitle/>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Samsung S9
                    </Typography>
                    <Typography>Price: 800$</Typography>
                    <Typography>Quantity: 0</Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" variant="contained" href="/">Back</Button>
                </CardActions>
            </Card>
        </Container>
    )
}

export default SamsungDetail