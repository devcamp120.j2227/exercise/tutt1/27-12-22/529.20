import { Container, Grid } from "@mui/material"
import { useState } from "react"
import LabelTitle from "../components/labelTitle"
import IphoneX from "../components/Order/IphoneOrderDetail"
import LabelPrice from "../components/Order/LabelPrice"
import Nokia from "../components/Order/NokiaOrderDetail"
import Samsung from "../components/Order/SamsungOrderDetail"

const Home = () => {
    const [ sumTotal, setSumTotal ] = useState(0);
       
    return (
        <Container>
            <LabelTitle/>
            <Grid container xs={12} spacing={3}>
                <Grid item xs={4}>
                    <IphoneX sumTotal={sumTotal} setSumTotal={setSumTotal}/>
                </Grid>
                <Grid item xs={4}>
                    <Samsung sumTotal={sumTotal} setSumTotal={setSumTotal}/>
                </Grid>
                <Grid item xs={4}>
                    <Nokia sumTotal={sumTotal} setSumTotal={setSumTotal}/>
                </Grid>
            </Grid>
            <LabelPrice sumTotal={sumTotal}/>
        </Container>
    )
}

export default Home