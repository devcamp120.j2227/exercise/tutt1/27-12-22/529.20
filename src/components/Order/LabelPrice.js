import { Typography } from "@mui/material"

const LabelPrice = ({sumTotal}) => {
    
    return (
        <Typography mt={5}>Total: {sumTotal} $</Typography>
    )
}

export default LabelPrice