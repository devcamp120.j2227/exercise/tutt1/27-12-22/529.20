import { Card, CardContent, Typography, Button, CardActions } from "@mui/material";
import { useState, useEffect } from "react";

const IphoneX = ({sumTotal, setSumTotal}) => {
    const [ iphone, setIphone ] = useState(0);
    const onBtnBuyClick = () => {
        setIphone(iphone + 1);
    } 

    useEffect(() => {
        if(iphone > 0) 
        setSumTotal(sumTotal + 900)
    },[iphone]);

    return (
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Iphone X
                </Typography>
                <Typography>Price: 900$</Typography>
                <Typography>Quantity: {iphone}</Typography>
            </CardContent>
            <CardActions>
                <Button size="small" variant="contained" onClick={onBtnBuyClick}>Buy</Button>&nbsp;
                <Button size="small" variant="contained" href="/iphonex">Detail</Button>
            </CardActions>
        </Card>
    )
}

export default IphoneX